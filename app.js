var express = require("express");
var path = require("path");
var logger = require("morgan");
var cors = require("cors");
require("dotenv").config();
const multer = require("multer");
var express = require("express");
var router = express.Router();
const database = require("./config/database");
const decode = require("base64-arraybuffer");
const fs = require("fs");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, "imgs/")); // Substitua pelo caminho real onde deseja armazenar os arquivos
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname); // Manter o nome original do arquivo
  },
});
const upload = multer({ storage: storage });

var pragasRoute = require("./app/routes/pragas");
var doencasRoute = require("./app/routes/doencas.js");
var produtosRoute = require("./app/routes/produtos");
var loginRoute = require("./app/routes/usuarios");
var culturasRoute = require("./app/routes/culturas");
var parteCulturaRoute = require("./app/routes/parte_cultura");

var app = express();

//midlewares
app.use(cors({ origin: "*" }));
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));

app.use(pragasRoute);
app.use(doencasRoute);
app.use(produtosRoute);
app.use(loginRoute);
app.use(culturasRoute);
app.use(parteCulturaRoute);

app.post("/file", upload.single("file"), async (req, res) => {
  const { nome } = req.body;
  const file = req.file;
  console.log(file);

  console;
  try {
    const fileData = fs.readFileSync(file.path, { encoding: "base64" });
    const { data, error } = await database.storage
      .from("tcc2")
      .upload(file.filename, fileData, {
        cacheControl: "3600",
        upsert: false,
      });
    console.log(data, "dataaa");
    console.log(error, "error");
  } catch (error) {
    console.error(error);
    res.status(500).send("Erro ao fazer upload do arquivo");
  }
});
module.exports = app;
