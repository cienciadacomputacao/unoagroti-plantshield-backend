var express = require("express");
var router = express.Router();
const database = require("../../config/database");

router.post("/doencas", async (req, res) => {
  try {
    if (!req.body.id_parte_cultura) {
      const { data } = await database.from("doencas").select();
      res.send(data);
    } else {
      const { data } = await database
        .from("doencas")
        .select()
        .eq("id_parte_cultura", req.body.id_parte_cultura);
      res.send(data);
    }
  } catch (error) {
    console.error(error);
    res.status(500).send("Erro ao buscar dados de doenças");
  }
});

router.post("/doencas/edit", async (req, res) => {
  try {
    const { data, error } = await database
      .from("doencas")
      .update(req.body)
      .eq("id", req.body.id);
    res.send(data);
  } catch (error) {
    console.error(error);
    res.status(500).send("Erro ao buscar dados de doenças");
  }
});

module.exports = router;
