var express = require("express");
var router = express.Router();
const database = require("../../config/database");

router.get("/culturas", async (req, res) => {
  try {
    const { data } = await database.from("culturas").select();
    res.send(data);
  } catch (error) {
    console.error(error);
    res.status(500).send("Erro ao buscar dados de culturas");
  }
});
module.exports = router;
