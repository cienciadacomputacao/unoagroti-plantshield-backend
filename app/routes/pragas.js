var express = require("express");
var router = express.Router();
const database = require("../../config/database");

router.post("/pragas", async (req, res) => {
  try {
    if (!req.body?.id_parte_cultura) {
      const { data } = await database.from("pragas").select();
      res.send(data);
    } else {
      const { data } = await database
        .from("pragas")
        .select()
        .eq("id_parte_cultura", req.body.id_parte_cultura);
      res.send(data);
    }
  } catch (error) {
    console.error(error);
    res.status(500).send("Erro ao buscar dados de pragas");
  }
});
router.post("/pragas/edit", async (req, res) => {
  try {
    const { data, error } = await database
      .from("pragas")
      .update(req.body)
      .eq("id", req.body.id);
    res.send(data);
  } catch (error) {
    console.error(error);
    res.status(500).send("Erro ao editar praga");
  }
});
module.exports = router;
