var express = require("express");
var router = express.Router();

router.get("/products", async (req, res) => {
  const { data, error } = await supabase.from("products").select();
  res.send(data);
});
