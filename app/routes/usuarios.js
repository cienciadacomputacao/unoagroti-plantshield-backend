var express = require("express");
var router = express.Router();
const supabase = require("../../config/database");

router.post("/login", async (req, res) => {
  try {
    const { data, error } = await supabase
      .from("usuarios")
      .select()
      .eq("email", req.body.email)
      .eq("senha", req.body.password);
    if (data.length === 0) {
      res.status(404).send("Nenhum usuário encontrado com essas credenciais.");
    } else {
      res.send(data);
    }
  } catch (error) {
    console.error(error);
    res.status(500).send("Erro ao buscar dados de produtos");
  }
});

router.post("/usuarios/create", async (req, res) => {
  try {
    const { data, error } = await supabase.from("usuarios").insert(req.body);
    console.log(data, "DATA");
    res.send(data);
  } catch (error) {
    console.error(error);
    res.status(500).send("Erro ao cadastrar novo usuário.");
  }
});
router.get("/usuarios", async (req, res) => {
  try {
    const { data, error } = await supabase.from("usuarios").select();
    res.send(data);
  } catch (error) {
    console.error(error);
    res.status(500).send("Erro ao buscar usuários.");
  }
});
router.post("/usuarios/update", async (req, res) => {
  try {
    const userId = parseInt(req.body.id); // Converte o ID para um número inteiro

    if (isNaN(userId)) {
      // Verifica se o ID é um número válido
      res.status(400).send("ID de usuário inválido.");
      return;
    }

    // Use o ID do usuário para filtrar o registro que você deseja atualizar
    const { data, error } = await supabase
      .from("usuarios")
      .update(req.body) // Aqui, você pode passar o corpo da solicitação diretamente para atualizar
      .eq("id", userId); // Filtra pelo ID do usuário

    res.send(data);
  } catch (error) {
    console.error(error);
    res.status(500).send("Erro ao editar usuário.");
  }
});

router.post("/usuarios/remove", async (req, res) => {
  try {
    const userId = parseInt(req.body.id); // Converte o ID para um número inteiro
    console.log(userId)
    if (isNaN(userId)) {
      // Verifica se o ID é um número válido
      res.status(400).send("ID de usuário inválido.");
      return;
    }

    // Use o ID do usuário para filtrar o registro que você deseja atualizar
    const { data, error } = await supabase
      .from("usuarios")
      .delete() // Aqui, você pode passar o corpo da solicitação diretamente para atualizar
      .eq("id", userId); // Filtra pelo ID do usuário

    res.send(data);
  } catch (error) {
    console.error(error);
    res.status(500).send("Erro ao remover usuário.");
  }
});
module.exports = router;
