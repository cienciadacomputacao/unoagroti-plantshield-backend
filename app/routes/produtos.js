var express = require("express");
var router = express.Router();
const database = require("../../config/database");

router.post("/produtos", async (req, res) => {
  try {
    const { type } = req.body;
    console.log(req.body);
    if (type === "agrobiologico") {
      const { data } = await database
        .from("produtos")
        .select()
        .eq("tipo", type);
      res.send(data);
    } else {
      if (req.body.nameOfProblem === "id_praga" && type === "biologico") {
        const { data } = await database
          .from("produtos")
          .select()
          .ilike("classe", ["%Inseticida%"]);
        res.send(data);
      } else if (
        req.body.nameOfProblem === "id_doenca" &&
        type === "biologico"
      ) {
        const { data } = await database
          .from("produtos")
          .select()
          .ilike("classe", ["%Fungicida%"]);
        res.send(data);
      } else {
        const { data } = await database
          .from("produtos")
          .select()
          .eq("tipo", type);
        if (data.length === 0) {
          const { data } = await database
            .from("produtos")
            .select()
            .eq("tipo", type)
            .eq(req.body.nameOfProblem, req.body.idPragaOrDoenca);
          res.send(data);
        }
        res.send(data);
      }
    }
  } catch (error) {
    console.error(error);
    res.status(500).send("Erro ao buscar dados de produtos");
  }
});
module.exports = router;
