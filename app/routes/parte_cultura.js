var express = require("express");
var router = express.Router();
const database = require("../../config/database");

router.get("/partecultura", async (req, res) => {
  try {
    const { data } = await database.from("parte_cultura").select();
    res.send(data);
  } catch (error) {
    console.error(error);
    res.status(500).send("Erro ao buscar dados da parte da cultura");
  }
});
module.exports = router;
