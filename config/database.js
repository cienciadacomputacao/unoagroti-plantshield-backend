const { createClient } = require("@supabase/supabase-js");

// Crie um cliente do Supabase usando o createClient
const database = createClient(
  process.env.SUPABASE_URL,
  process.env.SUPABASE_KEY
);
module.exports = database;
